package jakkar.wack.utilities;

import com.iwebpp.crypto.TweetNaclFast;
import jakkar.wack.commands.Command;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Set;

public class CommandHandler {
    private HashMap<String, Command> commands = new HashMap<String, Command>();
    private Logger logger = LoggerFactory.getLogger(CommandHandler.class);
    private String prefix = "!";

    public void genCommands() {
        Reflections reflections = new Reflections("jakkar.wack.commands");
        Set<Class<? extends Command>> commandClasses = reflections.getSubTypesOf(Command.class);

        for (Class<? extends Command> command : commandClasses) {
            try {
                Command commandObj = null;

                if (command.getSimpleName().equals("Help")) {
                    commandObj = command.getDeclaredConstructor(HashMap.class).newInstance(commands);
                } else {
                    commandObj = command.getDeclaredConstructor().newInstance();
                }

                commands.put(prefix + commandObj.getCommand(), commandObj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        logger.info("Generated commands");
    }

    public void checkCommand(MessageReceivedEvent event) {
        String[] msgArray = event.getMessage().getContentDisplay().split(" ");

        if ((msgArray.length == 1) && (commands.get(msgArray[0]) != null) && (event.getMessage().getContentDisplay().startsWith(prefix))) {
            commands.get(msgArray[0]).run(event, null);
            logger.info("Executed command: " + event.getMessage().getContentDisplay());
        } else if ((msgArray.length > 1) && (event.getMessage().getContentDisplay().startsWith(prefix))) {
            try {
                commands.get(msgArray[0]).run(event, msgArray[1]);
            } catch (Exception e) {
                logger.info("Couldn't run command " + msgArray[0]); // TODO: try to find a better way to do this,
                                                                    // idk feels hacky
            }
        }
    }
}
