package jakkar.wack;

import jakkar.wack.utilities.CommandHandler;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.login.LoginException;

public class Bot {
    private static Logger logger = LoggerFactory.getLogger(Bot.class);

    public static void main(String[] args) {
        CommandHandler commandHandler = new CommandHandler();

        try {
            JDA jda = new JDABuilder(args[0]) // bot token
                    .addEventListeners(new EventsManager(commandHandler)) // eventListener
                    .build();
            jda.awaitReady(); // wait until bot is ready
            logger.info("Finished building JDA!");

            commandHandler.genCommands();
        } catch (LoginException e) {
            e.printStackTrace(); // catch exception during authentication
        } catch (InterruptedException e) {
            e.printStackTrace(); // catch exception if .awaitReady() is interrupted
        }
    }
}
